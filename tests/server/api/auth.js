const axios = require('axios').default;
const config = require('../../../config.json');

let cookie = null;

describe('/api/auth/', () => {
  // Test [GET] /api/auth when user is not logged in.
  // We should expect an error saying that the user
  // has not been logged in yet.
  //
  // Expect:
  //    1. Responded with "Not logged in" error message.
  it('[GET] Get user info (not logged in)', done => {
    axios.get(`http://localhost:${config.http.port}/api/auth`)
      .then(res => {
        let { error, data } = res.data;
        if (error && error.includes('Not logged in'))
          return done();

        done(new Error(`No error message.`));
      })
      .catch(done);
  })

  // [POST] /api/auth to log user into the website.
  //
  // Make sure to save the cookie because we need it
  // for authentication later in this test.
  //
  // Expect:
  //    1. Responded with user's data.
  it('[POST] Login', (done) => {
    let params = {
      username: 'test@example.com',
      password: '123456'
    }
    axios.post(`http://localhost:${config.http.port}/api/auth/`, params)
      .then(res => {
        let { error, data } = res.data;
        if (error)
          return done(new Error(error));

        cookie = res.headers['set-cookie'];
        done();
      })
      .catch(err => {
        done(err);
      })
  })

  // [POST] /api/auth to log user into the website with the wrong
  // username/password combination.
  //
  // Expect:
  //    1. "Invalid username/password" error message.
  it('[POST] Invalid username/password', done => {
    let params = {
      username: 'haha@example.com',
      password: '123456'
    };
    axios.post(`http://localhost:${config.http.port}/api/auth/`, params)
      .then(res => {
        let { error, data } = res.data;
        if (error && error.includes('Invalid username/password'))
          return done();

        done(new Error('No error message.'));
      })
      .catch(err => {
        done(err);
      })
  })

  // [GET] /api/auth when user has already been logged in.
  //
  // Make sure to reuse the "cookie" saved previously.
  //
  // Expect: user's data is available.
  it('[GET] Get user info (logged in)', done => {
    axios.get(`http://localhost:${config.http.port}/api/auth`, {
      withCredentials: true,
      headers: {
        Cookie: cookie
      }
    })
      .then(res => {
        let { error, data } = res.data;
        if (!data)
          return done(new Error(`Response doesn't contain data.`));

        done();
      })
      .catch(done);
  })
})

// /api/auth/logout
describe('/api/auth/logout', () => {

  // [GET] /api/auth/logout to log the user out of the website.
  //
  // Expect:
  //    1. no error when [GET] /api/auth/logout
  //    2. [GET] /api/auth/ responds with error "Not logged in"
  it('[GET] Logout', done => {
    axios.get(`http://localhost:${config.http.port}/api/auth/logout`, {
      headers: {
        Cookie: cookie
      }
    })
      .then(res => {
        let { error, data } = res.data;
        if (error)
          return done(new Error(error));

        // Try getting the users again, see if it fails.
        axios.get(`http://localhost:${config.http.port}/api/auth/`, {
          headers: {
            Cookie: cookie
          }
        })
          .then(res => {
            let { error, data } = res.data;
            if (error && error.includes('Not logged in'))
              return done();

            return done(new Error('No error message.'));
          })
      })
      .catch(err => {
        done(err);
      })
  })
})
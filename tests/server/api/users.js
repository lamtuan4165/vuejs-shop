const config = require('../../../config.json');
const axios = require('axios').default;
const Client = require('pg').Client;
const format = require('pg-format');
const assert = require('assert');

const createUser = params => {
  return new Promise((resolve, reject) => {
    axios
      .post(`http://localhost:${config.http.port}/api/users/`, params)
      .then(res => resolve(res.data))
      .catch(reject);
  });
};

describe('/api/users/', () => {
  after(done => {
    console.log('Cleaning up database.');
    const db = new Client(config.database.postgres);
    db.connect(err => {
      if (err) return done(err);
      const query = format(`
        DELETE FROM users
        WHERE email='createUserTest@example.com'`);

      db.query(query)
        .then(res => {
          console.log('Database clean up successfully.');
          db.end();
          done();
        })
        .catch(done);
    });
  });

  it('[POST] Create user (success)', done => {
    let params = {
      first_name: 'First name test',
      last_name: 'Last name test',
      email: 'createUserTest@example.com',
      phone: '5105105106',
      password: '123456'
    };

    createUser(params)
      .then(msg => {
        let { error, data } = msg;
        if (error) return done(new Error(error));

        assert(data.id);
        assert(data.first_name == params.first_name);
        assert(data.last_name == params.last_name);
        assert(data.email == params.email);
        assert(!data.password);
        assert(data.created_on);
        assert(data.logged_on);

        done();
      })
      .catch(done);
  });

  it('[POST] Create user failure (duplicate email)', done => {
    let params = {
      first_name: 'First name test',
      last_name: 'Last name test',
      email: 'createUserTest@example.com',
      phone: '5105105103',
      password: '123456'
    };

    createUser(params)
      .then(msg => {
        let { error, data } = msg;

        if (!error) return done(new Error('Not supposed to go through.'));

        done();
      })
      .catch(done);
  });

  it('[POST] Create user failure (duplicate phone)', done => {
    let params = {
      first_name: 'First name test',
      last_name: 'Last name test',
      phone: '5105105106',
      password: '123456'
    };

    createUser(params)
      .then(msg => {
        let { error, data } = msg;

        if (!error) return done(new Error('Not supposed to go through.'));

        done();
      })
      .catch(done);
  });

  it('[POST] Create user failure (missing email)', done => {
    let params = {
      first_name: 'First name test',
      last_name: 'Last name test',
      phone: '5105105106',
      password: '123456'
    };
    createUser(params)
      .then(msg => {
        let { error, data } = msg;

        if (!error) return done(new Error('Not supposed to go through.'));

        done();
      })
      .catch(done);
  });

  it('[POST] Create user failure (missing phone)', done => {
    let params = {
      first_name: 'First name test',
      last_name: 'Last name test',
      email: 'createUserTest@example.com',
      password: '123456'
    };
    createUser(params)
      .then(msg => {
        let { error, data } = msg;

        if (!error) return done(new Error('Not supposed to go through.'));

        done();
      })
      .catch(done);
  });

  it('[POST] Create user failure (missing password)', done => {
    let params = {
      first_name: 'First name test',
      last_name: 'Last name test',
      email: 'createUserTest@example.com',
      phone: '5105105106'
    };
    createUser(params)
      .then(msg => {
        let { error, data } = msg;

        if (!error) return done(new Error('Not supposed to go through.'));

        done();
      })
      .catch(done);
  });
});

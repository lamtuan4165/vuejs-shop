const config = require('../../../config.json');
const axios = require('axios').default;
const Client = require('pg').Client;
const format = require('pg-format');
const assert = require('assert');

let productId = null;
let commonParams = {
  name: 'Some product name',
  description: 'product description',
  price: 10.0,
  inventory: 1,
  owner_id: 1
};

const createProduct = params => {
  return new Promise((resolve, reject) => {
    axios
      .post(`http://localhost:${config.http.port}/api/products/`, params)
      .then(res => resolve(res.data))
      .catch(reject);
  });
};

describe('/api/products/', () => {
  after(done => {
    console.log('Cleaning up database.');
    const db = new Client(config.database.postgres);
    db.connect(err => {
      if (err) return done(err);
      const query = format(
        `
        DELETE FROM products
        WHERE owner_id=%s`,
        commonParams.owner_id
      );

      db.query(query)
        .then(res => {
          console.log('Database clean up successfully.');
          db.end();
          done();
        })
        .catch(done);
    });
  });

  it('[POST] Add product (success)', done => {
    createProduct(commonParams)
      .then(msg => {
        let { error, data } = msg;
        if (error) return done(new Error(error));

        assert(data.id);
        assert(data.name == commonParams.name);
        assert(data.description == commonParams.description);
        assert(data.price == commonParams.price);
        assert(data.inventory == commonParams.inventory);
        assert(data.time);
        assert(data.owner_id == commonParams.owner_id);

        done();
      })
      .catch(done);
  });

  it('[POST] Add product failure (price is not a number)', done => {
    let params = { ...commonParams, price: 'a string' };

    createProduct(params)
      .then(msg => {
        let { error, data } = msg;

        if (!error) return done(new Error('Not supposed to go through.'));

        done();
      })
      .catch(done);
  });

  it('[POST] Add product failture (inventory is not a number)', done => {
    let params = { ...commonParams, inventory: 'a string' };

    createProduct(params)
      .then(msg => {
        let { error, data } = msg;

        if (!error) return done(new Error('Not supposed to go through.'));

        done();
      })
      .catch(done);
  });

  it('[POST] Add product failure (missing owner_id)', done => {
    let params = { ...commonParams, owner_id: null };
    createProduct(params)
      .then(msg => {
        let { error, data } = msg;

        if (!error) return done(new Error('Not supposed to go through.'));

        done();
      })
      .catch(done);
  });
});

import Encryptor from './Encryptor';

const { Client } = require('pg');
const format = require('pg-format');

export default class Database {
  constructor(config) {
    this.config = config;

    /** @type { Client } */
    this.client = new Client(config.database.postgres);
  }

  /**
   * Connect to database using credentials.
   * @returns
   */
  connect() {
    // TODO: Should make state machine.
    // Reason: Multiple connect() called at the same time?
    return new Promise((resolve, reject) => {
      this.client.connect()
        .then(resolve)
        .catch(reject)
    })
  }

  /**
   * Get a user from database.
   * Mostly used for looking at profiles.
   * @param {string} id
   */
  getUser(id) {
    return new Promise((resolve, reject) => {
      let query = format(`
      SELECT id, firstname, lastname, email, phone, created_on, logged_on
      FROM users
      WHERE id=%s`, id)

      this.client.query(query)
        // Expect only 1 profile.
        .then(res => resolve(res.rows[0]))
        .catch(reject);
    })
  }

  /**
   * Add a user into database.
   * TODO: Define details.
   * @param {*} details
   */
  addUser(details) {
    return new Promise((resolve, reject) => {
      let hashedPassword = Encryptor.hash(details.password);

      let query = format(`
      INSERT INTO users (first_name, last_name, email, phone, password)
      VALUES (%L, %L, %L, %L, %L)
      RETURNING id, first_name, last_name, email, phone , logged_on, created_on`, details.first_name, details.last_name,details.email, details.phone, hashedPassword);

      this.client.query(query)
        .then(data => resolve(data.rows[0]))
        .catch(reject);
    })

  }

  /**
   * Delete a user.
   * @param {*} details
   */
  deleteUser(details) {
    return new Promise((resolve, reject) => {
      let { id } = details;
      let query = format(`
      DELETE FROM users
      WHERE id=%s`, id)
      this.client.query(query)
        .then(res => resolve())
        .catch(reject);
    })
  }

  /**
   * Check the authentication.
   * @param {*} details
   */
  auth(details) {
    return new Promise((resolve, reject) => {
      let hashedPassword = Encryptor.hash(details.password);

      // Get all the user's information except the password.
      let query = format(`
      SELECT id, first_name, last_name, email, phone, created_on, logged_on
      FROM users
      WHERE (email=%L OR phone=%L) AND password=%L;
      `, details.username, details.username, hashedPassword)

      this.client.query(query)
        .then(res => resolve(res.rows[0]))
        .catch(reject);
    })
  }


  //-----Add product-----//
  
  addProduct(details) {
    return new Promise((resolve, reject) => {
      let query = format(
        `
      INSERT INTO products (name, description, price, inventory, owner_id)
      VALUES (%L, %L, %L, %L, %L)
      RETURNING id, name, description, price, inventory , time, owner_id
      `, 
        details.name, 
        details.description, 
        details.price, 
        details.inventory, 
        details.owner_id
      );

      this.client
        .query(query)
        .then(data => resolve(data.rows[0]))
        .catch(reject);
    });
  }
}
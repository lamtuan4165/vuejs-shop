const CryptoJS = require('crypto-js');
import config from '../../../config.json';

class Encryptor {
  /**
   * Hash a text.
   * @param {string} text
   */
  hash(text) {
    return CryptoJS.SHA256(text).toString();
  }

  /**
   * Encrypt a string. Please see config.json for secret.
   * @param {*} text Text to be encrypted.
   * @returns encryption
   */
  encrypt(text) {
    return CryptoJS.AES.encrypt(text, config.encryption.secret).toString();
  }

  /**
   * Decrypt a string.
   * @param {*} encryption
   * @returns
   */
  decrypt(encryption) {
    return CryptoJS.AES.decrypt(encryption, config.encryption.secret).toString(CryptoJS.enc.Utf8);
  }
}

export default new Encryptor;
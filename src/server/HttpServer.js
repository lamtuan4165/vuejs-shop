import api from './api';
import session from 'express-session';
import pgConnect from 'connect-pg-simple';

const express = require('express')

export default class HttpServer {
  constructor({ config, db}) {
    this.config = config;
    this.db = db;
    this.app = express();
  }

  start() {
    const { config, db } = this;
    this.initializeMiddleware();

    // URL: /api
    this.app.use('/api', api({ config, db }));

    this.app.listen(config.http.port, () => {
      console.log(`Server is listening on port ${config.http.port}`)
    })
  }

  initializeMiddleware() {
    // Intercept request messages and make it a JSON.
    // This helps turning requests into JSON object for
    // easy processing.
    this.app.use(express.json());

    // Manage sessions for authentication so that users
    // do not need re-login if the session is still active.
    const store = new (pgConnect(session))({
      conObject: this.config.database.postgres,
      createTableIfMissing: true
    })
    this.app.use(session({
      store,

      // A secret can be random. Usually fixed.
      secret: 'K3@cU@ZR8Gea',

      // TODO: What the hell are these 2 options?
      saveUninitialized: true,
      resave: false,

      cookie: {
        secure: false,
        httpOnly: false,
        sameSite: false,
        // The session expires in 1 day.
        maxAge: 1000 * 60 * 60 * 24,
      },
    }))
  }
}
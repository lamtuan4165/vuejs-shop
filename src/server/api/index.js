// Base URL: /api/

import auth from './auth';
import products from './products';
import users from './users';

const { Router } = require('express');

export default ({ config, db }) => {
  let router = Router();

  // Method: [GET]
  router.get('/', (req, res) => {
    res.json({ error: 'Unsupported' });
  })

  router.use('/users/', users({ config, db }));
  router.use('/auth/', auth({ config, db }))
  router.use('/products/', products({ config, db }))

  return router;
}
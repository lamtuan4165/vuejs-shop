// Base URL: /api/products/

const { Router } = require('express');

export default ({ config, db }) => {
  let router = Router();


  router.get('/', (req, res) => {
    return res.json({ error: 'Not supported' });
  });

  router.get('/:id', (req, res) => {
    return res.json({ error: 'Not supported' });
  });

  router.post('/', (req, res) => {
    let details = req.body;
    db.addProduct(details)
      .then(data => res.json({ success: true, data: data }))
      .catch(err => res.json({ error: err.message}));
  })

  return router;
};

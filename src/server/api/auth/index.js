// Base URL: /api/auth/
// Description: Manage authentication for logins.

import logout from './logout';

const { Router } = require('express');

export default ({ config, db }) => {
  let router = Router();

  // URL: /api/auth/logout/
  router.use('/logout', logout({ config, db }));

  // Get the current user.
  router.get('/', (req, res) => {
    if (!req.session.user) return res.json({ error: 'Not logged in.' });

    res.json({ success: true, data: req.session.user });
  });

  // Create a session.
  // This is used for allowing users to login.
  router.post('/', (req, res) => {
    // Get the credentials.
    // NOTE: User can either log in using Phone or Email.
    // We should support both and call it username from this point
    // on.
    let { username, password } = req.body;

    db.auth({ username, password })
      .then(data => {
        if (!data) return res.json({ error: 'Invalid username/password.' });

        // Make sure to set the session.user to indicate that
        // this user has been logged in.
        req.session.user = data;

        res.json({ success: true, data });
      })
      .catch(err => res.json({ error: err.message }));
  });

  return router;
};

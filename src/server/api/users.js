// Base URL: /api/users/

const { Router } = require('express');

export default ({ config, db }) => {
  let router = Router();

  // Method: [GET]
  router.get('/:id', (req, res) => {
    let { id } = req.params;

    db.getUser(id)
      .then(data => {
        if (!data)
          return res.json({ error: "User doesn't exist"}); 

        res.json({ success: true, data })
      })
      .catch(err => res.json({ error: err.message }))
  })

  // Create a new user.
  // TODO: need to define 'details'.
  router.post('/', (req, res) => {
    let details = req.body;
    db.addUser(details)
      .then(data => res.json({ success: true, data: data }))
      .catch(err => res.json({ error: err.message }));
  })

  return router;
}
const { VueLoaderPlugin } = require('vue-loader')
const CopyPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin')
const commonConfig = require('./common.config');
const path = require('path');
const srcPath = path.resolve(__dirname, '..', 'src', 'client')
const buildPath = path.resolve(__dirname, '..', 'build', 'dist')
const config = require('../config.json');

module.exports = Object.assign({
  entry: {
    main: path.resolve(srcPath, 'index.js')
  },
  output: {
    path: buildPath,
    filename: '[name].js',
    publicPath: '/'
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.css$/,
        exclude: /assets/,
        use: [
          'vue-style-loader',
          {
            loader: 'css-loader',
            options: { importLoaders: 1 }
          },
        ]
      }
    ]
  },
  devServer: {
    historyApiFallback: true,
    hot: true,
    compress: true,
    port: config.http.devPort,
    static: srcPath,
    proxy: [
      {
        context: ['/api'],
        target: `http://localhost:${config.http.port}`,
      }
    ]
  },
  plugins: [
    new VueLoaderPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: path.resolve(srcPath, 'assets', 'index.html')
    }),
    new CopyPlugin({
      patterns: [
        { from: path.join(srcPath, 'assets', 'css'), to: path.join(buildPath, 'css') },
        { from: path.join(srcPath, 'assets', 'js'), to: path.join(buildPath, 'js') }
      ]
    })
  ]
}, commonConfig)
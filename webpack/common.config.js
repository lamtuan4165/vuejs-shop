const path = require('path');

module.exports = {
	mode: process.env.NODE_ENV || 'development',
	stats: {
		colors: true
	},
	resolve: {
		alias: {
			'@': path.resolve(__dirname, '..')
		}
	},
	devtool: 'source-map'
}
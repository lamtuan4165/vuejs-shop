const nodeExternals = require('webpack-node-externals');
const commonConfig = require('./common.config');
const path = require('path');
const srcPath = path.resolve(__dirname, '..', 'src', 'server')
const buildPath = path.resolve(__dirname, '..', 'build')

module.exports = Object.assign({
	target: 'node',
	node: {
		__dirname: false,
		__filename: false
	},
	externals: [ nodeExternals() ],
	entry: {
		server: path.resolve(srcPath, 'index.js')
	},
	output: {
		path: buildPath,
		filename: '[name].js',
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				options: {
					presets: ['@babel/preset-env']
				}
			}
		]
	},
	plugins: []
}, commonConfig)